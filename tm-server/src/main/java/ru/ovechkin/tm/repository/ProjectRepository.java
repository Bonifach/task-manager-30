package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public final void add(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Query query = entityManager.createQuery(
                "from Project where user_id = :userId and id = :id", Project.class);
        query.setParameter("userId", userId);
        query.setParameter("id", id);
        @NotNull final Project project = (Project) query.getSingleResult();
        return project;
    }

    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Query query = entityManager.createQuery(
                "from Project where user_id = :userId and name = :name", Project.class);
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        try {
            @NotNull final Project project = (Project) query.getSingleResult();
            return project;
        } catch (NoResultException e) {
            return null;
        }
    }

    @Nullable
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final Query query = entityManager.createQuery(
                "from Project where user_id = :userId", Project.class);
        query.setParameter("userId", userId);
        if (query.getResultList() == null || query.getResultList().isEmpty()) return null;
        @Nullable final List<Project> projects = (List<Project>) query.getResultList();
        return projects;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final Query query = entityManager.createQuery("delete Project where user_id = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        entityManager.remove(project);
        return project;
    }


    @Nullable
    @Override
    public List<Project> getAllProjects() {
        @Nullable final Query query = entityManager.createQuery("from Project", Project.class);
        return (List<Project>) query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> mergeCollection(@NotNull final List<Project> projectList) {
        for (@NotNull final Project project : projectList) {
            entityManager.persist(project);
        }
        return projectList;
    }

    @Override
    public void removeAllProjects() {
        final Query query = entityManager.createQuery("delete from Project", Project.class);
        query.executeUpdate();
    }

}