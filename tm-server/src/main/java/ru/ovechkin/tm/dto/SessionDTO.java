package ru.ovechkin.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jetbrains.annotations.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public final class SessionDTO extends AbstractDTO implements Cloneable {

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getStackTrace());
            return null;
        }
    }

    @NotNull
    private Long timestamp;

    @NotNull
    private String userId;

    @NotNull
    private String signature;

    public SessionDTO() {
    }

    @Override
    public String toString() {
        return "Session{" +
                "timestamp=" + timestamp +
                ", userId='" + userId + '\'' +
                ", signature='" + signature + '\'' +
                '}';
    }

    @NotNull
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@NotNull Long timestamp) {
        this.timestamp = timestamp;
    }

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @NotNull
    public String getSignature() {
        return signature;
    }

    public void setSignature(@NotNull String signature) {
        this.signature = signature;
    }

}