package ru.ovechkin.tm.locator;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.repository.ProjectRepository;
import ru.ovechkin.tm.repository.SessionRepository;
import ru.ovechkin.tm.repository.TaskRepository;
import ru.ovechkin.tm.repository.UserRepository;
import ru.ovechkin.tm.service.*;

import javax.persistence.EntityManager;

public class ServiceLocator implements IServiceLocator {

    @NotNull
    final IServiceLocator serviceLocator = this;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

//    @NotNull
//    private final IUserRepository userRepository = new UserRepository();

//    @NotNull
//    private final IUserService userService = new UserService(userRepository, serviceLocator);
    @NotNull
    private final IUserService userService = new UserService(serviceLocator);

    @NotNull
    private final ITaskService taskService = new TaskService(serviceLocator);

    @NotNull
    private final IProjectService projectService = new ProjectService(serviceLocator);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);


    @NotNull
    private final ISessionService sessionService = new SessionService(serviceLocator);

    @NotNull
    private final IStorageService storageService = new StorageService(serviceLocator);

    @NotNull
    private final IEntityManagerService entityManagerService = new EntityManagerService(serviceLocator);

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IStorageService getStorageService() {
        return storageService;
    }

    @NotNull
    @Override
    public IEntityManagerService getEntityManagerService() {
        return entityManagerService;
    }
}