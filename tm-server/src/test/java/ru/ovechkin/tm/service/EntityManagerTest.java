package ru.ovechkin.tm.service;
/*
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.entity.TestEntity;
import ru.ovechkin.tm.locator.ServiceLocator;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

public class EntityManagerTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEntityManagerService emf = new EntityManagerService(serviceLocator);

    @Test
    public void testJPQLQueryInsert() {
        final EntityManager em = emf.getEntityManager();
        final TestEntity testEntity = new TestEntity();
        testEntity.setName("testName2");
        testEntity.setDescription("testDescription2");
        em.getTransaction().begin();
        em.createNativeQuery("INSERT INTO app_test_entity (id, name, description) VALUES (?,?,?)")
                .setParameter(1, testEntity.getId())
                .setParameter(2, testEntity.getName())
                .setParameter(3, testEntity.getDescription())
                .executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testJPQLQuerySelect() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();
        final Query query = em.createQuery("SELECT a FROM TestEntity a WHERE name = :name")
                .setParameter("name", "testPersist");
        final TestEntity testEntity = (TestEntity) query.getSingleResult();
        System.out.println(testEntity.getDescription());
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testHQLQuerySelect() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
//        em.getTransaction().begin();
        final Query query = em.createQuery(
                "from TestEntity where name = :name", TestEntity.class);
        query.setParameter("name", "testFlush");
        final TestEntity testEntity = (TestEntity) query.getSingleResult();
        System.out.println(testEntity.getDescription());
//        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testHqlJpqlQueryUpdate() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();
        final Query query = em.createQuery(
                "update TestEntity set name = :newName, description = :newDescription" +
                        " where name = :name");
        query.setParameter("name", "testName");
        query.setParameter("newName", "newTestName");
        query.setParameter("newDescription", "newTestDescription");
        query.executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testHQLQueryRemove() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();
        final Query query = em.createQuery(
                "delete TestEntity where name = :name");
        query.setParameter("name", "newTestName");
        query.executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testHQLQueryRemove2() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();
        final Query query = em.createQuery(
                "delete from TestEntity where name = :name");
        query.setParameter("name", "testPersist");
        query.executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testPersist() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();
        final TestEntity testEntity = new TestEntity();
        testEntity.setName("testPersist");
        testEntity.setDescription("testPersist");
        em.persist(testEntity);
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testFind() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        final TestEntity testEntity = em.find(TestEntity.class, "dd4862eb-39e7-438e-b10c-44d5d8e9758e");
        System.out.println(testEntity.getName());
        em.close();
    }

    @Test
    public void testMergeAsUpdate() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();

        final TestEntity testEntity = new TestEntity();
        testEntity.setName("testMergeAsUpdate");
        testEntity.setDescription("testMergeAsUpdate");
        em.merge(testEntity);
        em.getTransaction().commit();
        em.clear();

        em.getTransaction().begin();
        testEntity.setName("updatedWithMerge");
        em.merge(testEntity);
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testRemove() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();

        final TestEntity testEntity = new TestEntity();
        testEntity.setName("testRemove");
        testEntity.setDescription("testRemove");
        em.persist(testEntity);
        em.getTransaction().commit();
        em.clear();

        System.out.println(em.find(TestEntity.class, testEntity.getId()).getName());

        em.getTransaction().begin();
        em.remove(em.find(TestEntity.class, testEntity.getId()));
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testFlushRefresh() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        em.getTransaction().begin();

        final TestEntity testEntity = new TestEntity();
        testEntity.setName("testFlush");
        testEntity.setDescription("testFlush");
        em.persist(testEntity);
        em.getTransaction().commit();
        em.clear();

        testEntity.setName("newTestFlush");
        System.out.println(testEntity.getName());
        em.getTransaction().begin();

//        em.flush(); //смывка сикуль запроса, чтобы обновить состояние с базой
        em.refresh(testEntity);

        System.out.println(testEntity.getName());
        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void testCriteriaBuilderSelect() {
        emf.init();
        final CriteriaBuilder criteriaBuilder = emf.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<TestEntity> criteriaQuery = criteriaBuilder.createQuery(TestEntity.class);
        criteriaQuery.from(TestEntity.class);

        System.out.println(emf.getEntityManager().createQuery(criteriaQuery).getResultList());
        emf.getEntityManager().close();
    }

    @Test
    public void testCriteriaBuilderSelectCount() {
        emf.init();
        final CriteriaBuilder criteriaBuilder = emf.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        final Root<TestEntity> root = criteriaQuery.from(TestEntity.class);
        criteriaQuery.select(criteriaBuilder.count(root));
        System.out.println(emf.getEntityManager().createQuery(criteriaQuery).getSingleResult());
        emf.getEntityManager().close();
    }

    @Test
    public void testCriteriaBuilderSelectWhere() {
        emf.init();
        final CriteriaBuilder criteriaBuilder = emf.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<TestEntity> criteriaQuery = criteriaBuilder.createQuery(TestEntity.class);
        final Root<TestEntity> root = criteriaQuery.from(TestEntity.class);

        final Path<String> path = root.get("id");
        final ParameterExpression<String> parameterExpression = criteriaBuilder.parameter(String.class, "id");
        final Predicate predicateById = criteriaBuilder.equal(path, parameterExpression);
        criteriaQuery.where(predicateById);

        final TypedQuery<TestEntity> typedQuery = emf.getEntityManager().createQuery(criteriaQuery);
        typedQuery.setParameter("id", "6b1bf518-658d-4708-b696-d751112430e7");
        typedQuery.setMaxResults(1);
//        System.out.println(ge);
        emf.getEntityManager().close();
    }

    @Test
    public void testFindAll() {
        emf.init();
        final EntityManager em = emf.getEntityManager();
        final Query query = em.createQuery("from TestEntity", TestEntity.class);
        final List<TestEntity> testEntities = (List<TestEntity>) query.getResultList();
        System.out.println(testEntities.toString());
    }
}
*/