package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.UserDTO;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.UPDATE_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Update you profile";
    }

    @Override
    public void execute() {
        @Nullable final UserDTO userDTO = endpointLocator.getSessionEndpoint().getUser(sessionDTO);
        System.out.println("[UPDATE PROFILE INFO]");
        System.out.println("YOUR CURRENT LOGIN IS: " + "[" + userDTO.getLogin() + "]");
        System.out.print("ENTER NEW LOGIN: ");
        final String newLogin = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT FIRST NAME IS: " + "[" + userDTO.getFirstName() + "]");
        System.out.print("ENTER NEW FIRST NAME: ");
        final String newFirstName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT MIDDLE NAME IS: " + "[" + userDTO.getMiddleName() + "]");
        System.out.print("ENTER NEW MIDDLE NAME: ");
        final String newMiddleName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT LAST NAME IS: " + "[" + userDTO.getLastName() + "]");
        System.out.print("ENTER NEW LAST NAME: ");
        final String newLastName = TerminalUtil.nextLine();
        System.out.println("YOUR CURRENT EMAIL IS: " + "[" + userDTO.getEmail() + "]");
        System.out.print("ENTER NEW EMAIL NAME: ");
        final String newEmail = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().updateProfileLogin(sessionDTO, newLogin);
        System.out.println("[COMPLETE]");
    }

}