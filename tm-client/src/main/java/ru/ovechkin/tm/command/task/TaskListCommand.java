package ru.ovechkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.TaskDTO;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_TASK_LIST;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        @NotNull final List<TaskDTO> tasksDTO =
                endpointLocator.getTaskEndpoint().findUserTasks(sessionDTO);
        @NotNull int index = 1;
        for (@NotNull final TaskDTO taskDTO : tasksDTO) {
            System.out.println(index + ". " + taskDTO);
            index++;
        }
        System.out.println("[OK]");
    }

}