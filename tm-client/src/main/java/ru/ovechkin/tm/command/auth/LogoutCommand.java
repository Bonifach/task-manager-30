package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.SessionEndpoint;

public class LogoutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        @NotNull final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();
        sessionEndpoint.signOutByUserId(sessionDTO.getUserId());
        sessionDTO.setId("");
        sessionDTO.setUserId("");
        sessionDTO.setSignature("");
        sessionDTO.setTimestamp(0L);
        System.out.println("[OK]");
    }

}