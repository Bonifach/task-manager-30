package ru.ovechkin.tm.locator;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.endpoint.*;

public class EndpointLocator implements IEndpointLocator {

    private final UserEndpointService userEndpointService = new UserEndpointService();

    private final UserEndpoint userEndpoint = userEndpointService.getUserEndpointPort();


    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();


    private final StorageEndpointService storageEndpointService = new StorageEndpointService();

    private final StorageEndpoint storageEndpoint = storageEndpointService.getStorageEndpointPort();


    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();


    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();


    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public StorageEndpoint getStorageEndpoint() {
        return storageEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

}